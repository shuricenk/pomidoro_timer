# encoding: utf-8
from tkinter import *
from datetime import datetime
from tkinter.messagebox import *

temp = 1500
after_id = 0


def tick():
    global temp, after_id
    after_id = root.after(1000, tick)
    f_temp = datetime.fromtimestamp(temp).strftime("%M:%S")
    d_label.configure(text=str(f_temp))
    temp -= 1
    if f_temp == "00:00":
        stop_tm()
        d_label.configure(text="00:00")
        print("\a")
        res = askokcancel("Перерыв", "Сделать перерыв?")
        if res:
            duration_5()
            start_tm()


def start_tm():
    tick()

    btn_start.pack_forget()
    btn_25min.pack_forget()
    btn_5min.pack_forget()
    btn_stop.pack(side=LEFT, fill=X)
    btn_25min.pack(side=LEFT, fill=X)
    btn_5min.pack(side=LEFT, fill=X)


def stop_tm():
    root.after_cancel(after_id)

    btn_stop.pack_forget()
    btn_25min.pack_forget()
    btn_5min.pack_forget()
    btn_start.pack(side=LEFT, fill=X)
    btn_25min.pack(side=LEFT, fill=X)
    btn_5min.pack(side=LEFT, fill=X)


def duration_25():
    global temp
    temp = 1500
    t_temp = datetime.fromtimestamp(temp).strftime("%M:%S")
    d_label.configure(text=t_temp)


def duration_5():
    global temp
    temp = 300
    t_temp = datetime.fromtimestamp(temp).strftime("%M:%S")
    d_label.configure(text=t_temp)


root = Tk()
root.resizable(0, 0)
root.title('Timer')

d_temp = datetime.fromtimestamp(temp).strftime("%M:%S")
d_label = Label(root, width=5, font=("UBUNTU", 50), text=d_temp, bg='white')
d_label.pack(fill=X)

btn_start = Button(root, text='Start', font=("UBUNTU", 10),
                   command=start_tm, width=6, bg='white')
btn_stop = Button(root, text='Stop', font=("UBUNTU", 10),
                  command=stop_tm, width=6, bg='white')
btn_25min = Button(root, text='25 Min', font=("UBUNTU", 10),
                   command=duration_25, width=6, bg='white')
btn_5min = Button(root, text='5 Min', font=("UBUNTU", 10),
                  command=duration_5, width=6, bg='white')

btn_start.pack(side=LEFT, fill=X)
btn_25min.pack(side=LEFT, fill=X)
btn_5min.pack(side=LEFT, fill=X)

root.mainloop()
